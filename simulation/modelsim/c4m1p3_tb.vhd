LIBRARY ieee  ; 
LIBRARY std  ; 
USE ieee.std_logic_1164.all  ; 
USE ieee.std_logic_textio.all  ; 
USE ieee.std_logic_unsigned.all  ; 
USE std.textio.all  ; 
ENTITY c4m1p3_tb  IS 
END ; 
 
ARCHITECTURE c4m1p3_tb_arch OF c4m1p3_tb IS
  SIGNAL A   :  std_logic_vector (3 downto 0)  ; 
  SIGNAL B   :  std_logic_vector (3 downto 0)  ; 
  SIGNAL Cin   :  STD_LOGIC  ; 
  SIGNAL Cout   :  STD_LOGIC  ; 
  SIGNAL S   :  std_logic_vector (3 downto 0)  ; 
  COMPONENT C4M1P3  
    PORT ( 
      A  : in std_logic_vector (3 downto 0) ; 
      B  : in std_logic_vector (3 downto 0) ; 
      Cin  : in STD_LOGIC ; 
      Cout  : out STD_LOGIC ; 
      S  : out std_logic_vector (3 downto 0) ); 
  END COMPONENT ; 
BEGIN
  DUT  : C4M1P3  
    PORT MAP ( 
      A   => A  ,
      B   => B  ,
      Cin   => Cin  ,
      Cout   => Cout  ,
      S   => S   ) ; 



-- "Counter Pattern"(Range-Up) : step = 1 Range(0-1)
-- Start Time = 0 ps, End Time = 1 us, Period = 50 ps
Process
	variable VARCin  : std_logic_vector(0 downto 0);
	Begin
		for Z in 1 to 10 loop
			VARCin  := "0" ;
			for repeatLength in 1 to 2 loop
				Cin  <= VARCin (0)  ;
				wait for 1 ns ;
				VARCin  := VARCin  + 1 ;
			end loop;
	-- 1 ns, repeat pattern in loop.
		end  loop;
	wait;
End Process;


-- "Counter Pattern"(Range-Up) : step = 1 Range(0000-1111)
-- Start Time = 0 ps, End Time = 1 ns, Period = 50 ns
  Process
	variable VARA  : std_logic_vector (3 downto 0);
	Begin
	 A  <= "0000"  ;
	 for i in 0 to 31 loop
		A <= i;
		wait for 1 ns ;
	 end loop;
	wait;
 End Process;


-- "Counter Pattern"(Range-Up) : step = 1 Range(0000-1111)
-- Start Time = 0 ps, End Time = 1 ns, Period = 50 ns
  Process
	variable VARB  : std_logic_vector (3 downto 0);
	Begin
	 B  <= "0000"  ;
	for j in 0 to 31 loop
		B <= j;
		wait for 1 ns ;
	 end loop;
	wait;
 End Process;
 
END architecture c4m1p3_tb_arch;
