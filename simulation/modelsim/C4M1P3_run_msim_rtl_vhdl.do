transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/AlteraPrj/C4M1P3 {C:/AlteraPrj/C4M1P3/DE10_LITE_Golden_Top.v}
vcom -93 -work work {C:/AlteraPrj/C4M1P3/C4M1P3.vhd}
vcom -93 -work work {C:/AlteraPrj/C4M1P3/FullAdder.vhd}

vcom -93 -work work {C:/AlteraPrj/C4M1P3/simulation/modelsim/c4m1p3_tb.vhd}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L fiftyfivenm -L rtl_work -L work -voptargs="+acc"  tb

add wave *
view structure
view signals
run -all
