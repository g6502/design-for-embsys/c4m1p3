library ieee;
use ieee.std_logic_1164.all;

entity C4M1P3 is port 
	(
		SW		: IN	std_logic_vector (9 downto 0);
		LEDR	: OUT	std_logic_vector (9 downto 0)
	);
end entity C4M1P3;

architecture FourBitAdder of C4M1P3 is
	
	--signal Cin	:	std_logic := SW(8);
	--signal A		:	std_logic_vector (3 downto 0) := SW(7 downto 4);
	--signal B		:	std_logic_vector (3 downto 0) := SW(3 downto 0);
	--signal S		:	std_logic_vector (3 downto 0);
	--signal Cout	:	std_logic;
	signal Carry:	std_logic_vector (2 downto 0);
	
	component FullAdder is port
		(
			Cin	: IN	std_logic;
			A		: IN	std_logic;
			B		: IN	std_logic;
			S		: OUT	std_logic;
			Cout	: OUT	std_logic
		);
	end component;

begin
	
	U1	: FullAdder port map (Cin=>SW(8)		, A=>SW(4), B=>SW(0), S=>LEDR(0), Cout=>Carry(0));
	U2	: FullAdder port map (Cin=>Carry(0)	, A=>SW(5), B=>SW(1), S=>LEDR(1), Cout=>Carry(1));
	U3	: FullAdder port map (Cin=>Carry(1)	, A=>SW(6), B=>SW(2), S=>LEDR(2), Cout=>Carry(2));
	U4	: FullAdder port map (Cin=>Carry(2)	, A=>SW(7), B=>SW(3), S=>LEDR(3), Cout=>LEDR(4));
	
	--U1	: FullAdder port map (Cin=>Cin		, A=>A(0), B=>B(0), S=>S(0), Cout=>Carry(0));
	--U2	: FullAdder port map (Cin=>Carry(0)	, A=>A(1), B=>B(1), S=>S(1), Cout=>Carry(1));
	--U3	: FullAdder port map (Cin=>Carry(1)	, A=>A(2), B=>B(2), S=>S(2), Cout=>Carry(2));
	--U4	: FullAdder port map (Cin=>Carry(2)	, A=>A(3), B=>B(3), S=>S(3), Cout=>Cout);
	--LEDR <= "00000" & Cout & S;
	
end architecture FourBitAdder;