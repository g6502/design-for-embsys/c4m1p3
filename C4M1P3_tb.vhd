library ieee;
use ieee.std_logic_1164.all;

entity C4M1P3_tb is port 
end entity C4M1P3_tb;

architecture tb of C4M1P3_tb is

	signal Carry: std_logic_vector(2 downto 0);
	signal Cin	: IN	std_logic;
	signal A	: IN	std_logic_vector (3 downto 0);
	signal B	: IN	std_logic_vector (3 downto 0);
	signal S	: OUT	std_logic_vector (3 downto 0);
	signal Cout	: OUT	std_logic;
	
	component FullAdder is port
		(
			Cin	: IN	std_logic;
			A		: IN	std_logic;
			B		: IN	std_logic;
			S		: OUT	std_logic;
			Cout	: OUT	std_logic
		);
	end component;

begin
	UUT : entity work.C4M1P3 port map (Cin=>Cin, A=>A, B=>B, S=>S, Cout=>Cout);
	tb1 : process
	begin
		for i in 31'range loop
			for j in 31'range loop
				
				A <= i;
				B <= j;
				wait for 20ns;
				assert(
					S=i+j+Cin
				)
				report  "test_vector " & integer'image(i) & " failed " & 
                    " for input a = " & std_logic'image(j) & 
                    " and b = " & std_logic'image(S)
                    severity error;
			end loop;
		end loop;
		
	end process;
end architecture tb;